# Get current directory.
rootdir0 = @__DIR__

using Test

@testset "Periods" begin
    include("types.jl")
    include("weeks.jl")
end

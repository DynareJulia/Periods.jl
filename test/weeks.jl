# Taken from https://en.wikipedia.org/wiki/ISO_week_date (Long years per 400-year leap-cycle)
LongYears = [4; 9 ; 15 ; 20 ; 26 ; 32 ; 37 ; 43 ; 48 ; 54 ; 60 ; 65 ; 71 ; 76 ; 82 ; 88 ; 93 ; 99 ;
             105 ; 111 ; 116 ; 122 ; 128 ; 133 ; 139 ; 144 ; 150 ; 156 ; 161 ; 167 ; 172 ; 178 ; 184 ; 189 ; 195 ;
             201 ; 207 ; 212 ; 218 ; 224 ; 229 ; 235 ; 240 ; 246 ; 252 ; 257 ; 263 ; 268 ; 274 ; 280 ; 285 ; 291 ; 296 ;
             303 ; 308 ; 314 ; 320 ; 325 ; 331 ; 336 ; 342 ; 348 ; 353 ; 359 ; 364 ; 370 ; 376 ; 381 ; 387 ; 392 ; 398]

@testset "Long weeks (ISO-8601)" begin
    @test begin
        try
            Periods.islongyear(2020)
        catch
            false
        end
    end
    @test begin
        try
            !Periods.islongyear(2021)
        catch
            false
        end
    end
    @test begin
        try
            all(Periods.islongyear.(LongYears))
        catch
            false
        end
    end
    @test begin
        try
            Periods.weeksinyear(2020)==53
        catch
            false
        end
    end
    @test begin
        try
            Periods.weeksinyear(2021)==52
        catch
            false
        end
    end
    @test begin
        try
            all(Periods.weeksinyear.(LongYears).==53)
        catch
            false
        end
    end
    @test begin
        try
            all(Periods.weeksinyear.(setdiff(1:400,LongYears)).==52)
        catch
            false
        end
    end
end

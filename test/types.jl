using Periods

@testset "Instantiate daily Periods" begin
    @test begin
        try
            Periods.Period(2009,4,12)
            true
        catch
            false
        end
    end
    @test begin
        using Dates
        try
            d = Date(2009,4,12)
            Periods.Period(d)
            true
        catch
            false
        end
    end
    @test begin
        using Dates
        try
            Periods.Period(Date(2009,4,12))[1]==Periods.Period(2009,4,12)[1]
        catch
            false
        end
    end
    @test begin
        using Dates
        try
            Periods.Period(Date(2009,4,12))[2]==Periods.Day
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,4,12)[2]==Periods.Day
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,4,32)
            false
        catch
            true
        end
    end
    @test begin
        try
            Periods.Period(2001,2,29)
            false
        catch
            true
        end
    end
    @test begin
        try
            Periods.Period(2000,2,29)
            true
        catch
            false
        end
    end
end

@testset "Instantiate quarterly Periods" begin
    @test begin
        try
            Periods.Period(2009,2,Periods.Quarter)
            true
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,2,Periods.Quarter).frequency==Periods.Quarter
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,2,Periods.Quarter).ordinal::Int64
            true
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,5,Periods.Quarter)
            false
        catch
            true
        end
    end
end

@testset "Instantiate weekly Periods" begin
    @test begin
        try
            Periods.Period(2009,15,Periods.Week)
            true
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,15,Periods.Week).frequency==Periods.Week
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,15,Periods.Week).ordinal::Int64
            true
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,54,Periods.Week)
            false
        catch
            true
        end
    end
end

@testset "Instantiate monthly Periods" begin
    @test begin
        try
            Periods.Period(2009,4,Periods.Month)
            true
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,4,Periods.Month).frequency==Periods.Month
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,4,Periods.Month).ordinal::Int64
            true
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,13,Periods.Month)
            false
        catch
            true
        end
    end
end

@testset "Instantiate bi-annual Periods" begin
    @test begin
        try
            Periods.Period(2009,1,Periods.Semester)
            true
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,1,Periods.Semester).frequency==Periods.Semester
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,1,Periods.Semester).ordinal::Int64
            true
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,3,Periods.Semester)
            false
        catch
            true
        end
    end
end

@testset "Instantiate annual Periods" begin
    @test begin
        try
            Periods.Period(2009,Periods.Year)
            true
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,Periods.Year).frequency==Periods.Year
        catch
            false
        end
    end
    @test begin
        try
            Periods.Period(2009,Periods.Year).ordinal::Int64
            true
        catch
            false
        end
    end
end

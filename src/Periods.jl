module Periods
using Dates
abstract type AbstractPeriod end

include("SimplePeriods.jl")
export Frequency, Period, Year, Semester, Quarter, Month, Week, Day, Undated

import Base.:-
-(p1::AbstractPeriod, p2::AbstractPeriod) = p1.ordinal - p2.ordinal

import Base.:>
>(p1::AbstractPeriod, p2::AbstractPeriod) = p1.ordinal > p2.ordinal
import Base.:<
<(p1::AbstractPeriod, p2::AbstractPeriod) = p1.ordinal < p2.ordinal

import Base.checkindex
checkindex(::Type{Bool}, inds::AbstractUnitRange, p::AbstractPeriod) = checkindex(Bool, inds, p.ordinal)

import Base.isless
isless(p1::AbstractPeriod, p2::AbstractPeriod) = p1.ordinal < p2.ordinal

end # module
